import json
import logging
import requests
import tarfile
from datetime import timedelta
from pathlib import Path
from time import time, sleep

logging_fmt = '%(asctime)s %(levelname)s' \
                  ' %(name)s # %(module)s.%(funcName)s:%(lineno)s "%(message)s"'

logging_file_lvl = logging.DEBUG
logging_console_lvl = logging.DEBUG

logger = logging.getLogger(__name__)


class SProxyExecution:

    from .common import JobState

    logger = logger.getChild(__qualname__)

    def __init__(self, url, blob_service, job_service, label, work_dir,
        arguments=None, inputs_map=None, outputs_map=None, blobs_map=None,
                                                               keep_blobs=True):

        logger_name = url.replace('.', '_')
        self.logger = self.__class__.logger.getChild(url)

        self.url = url
        self.blob_service = blob_service
        self.job_service = job_service

        self.label = label
        self.work_dir = Path(work_dir)

        self.arguments = arguments or dict()
        self.inputs_map = inputs_map or dict()
        self.outputs_map = outputs_map or dict()
        self.existing_blobs_map = blobs_map or dict()
        self.keep_blobs = keep_blobs

        self.blobs_map = None
        self.job_url = None

        self.logger.debug('INITED!')

    # TODO: implement blob existence
    # def _blob_exists(self, reference):
    #     return
    #     existing_blob = self.existing_blobs_map.get(input_reference)
    #     response = requests.get(f'{blob_service_url}/{existing_blob}')

    def prepare(self):
        if self.blobs_map:
            raise RuntimeError('blobs map already exists!')

        blob_service_url = f'{self.url}/blob/{self.blob_service}'

        blobs_map = dict()
        for input_reference, input_name in self.inputs_map.items():

            # if _blob_exists(input_reference):
            #     continue

            input_path = self.work_dir / input_name
            with input_path.open(mode='rb') as input_file:

                blob_file_objects = { input_reference: input_file }
                response = requests.put(blob_service_url,
                                                        files=blob_file_objects)

            if not response.ok:
                raise RuntimeError(
                     f'something went wrong while uploading {input_reference}!')

            blobs_map.update( response.json() )

        self.blobs_map = blobs_map
        self.logger.debug('blobs map: %s', self.blobs_map)

        self.logger.info('PREPARED!')
        return self

    # def _job_service_url(self, with_suffix=None):
    #     common_url = f'{self.url}/job/{self.job_service}'
    #     return f'{common_url}/{with_suffix}' if with_suffix else common_url

    def submit(self):
        if self.job_url:
            raise RuntimeError('job url already exists!')

        if not self.blobs_map:
            raise RuntimeError('no blobs map!')

        self.logger.debug('submitting job labeled %s!', self.label)

        with requests.Session() as job_session:
            job_service_url = f'{self.url}/job/{self.job_service}/{self.label}'
            response = job_session.post(job_service_url,
                json = dict(
                    inputs = self.blobs_map,
                    arguments = self.arguments,
                ),
            )

        self.logger.info('submitted job labeled %s!', self.label)

        job_url = response.headers.get('location')
        self.logger.debug('job url: %s', job_url)

        if not job_url:
            raise RuntimeError('no job url!')

        self.job_url = job_url
        self.logger.debug('SUBMITTED!')
        return self

    wait_refresh = timedelta(seconds=60)
    wait_timeout = timedelta(hours=24)

    def wait(self):
        if not self.job_url:
            raise RuntimeError('no job url!')

        warm_up_secs = 10
        self.logger.debug('warming up... (%d secs)', warm_up_secs)

        begin_time = time()
        sleep(warm_up_secs)

        with requests.Session() as wait_session:
            refresh_secs = self.wait_refresh.total_seconds()

            while True:
                response = wait_session.get(self.job_url)

                wait_duration_secs = time() - begin_time
                wait_duration_td = timedelta(seconds=wait_duration_secs)

                if response.text == self.JobState.exited:
                    self.logger.info('%s exited (%s)!', self.job_url,
                                                               wait_duration_td)
                    break

                if wait_duration_td >= self.wait_timeout:
                    self.logger.warning('%s timed out (%s)!', self.job_url,
                                                               wait_duration_td)
                    raise RuntimeError(
                              f'{self.job_url} timed out ({wait_duration_td})!')

                self.logger.debug('%s is still running (%s)', self.job_url,
                                                               wait_duration_td)
                sleep(refresh_secs)

        self.logger.info('WAITED!')
        return self

    fetch_chunk_size = 16 * 1024 # 16KiB

    def fetch(self):
        if not self.job_url:
            raise RuntimeError('no job url!')

        with requests.Session() as fetch_session:
            fetch_session.stream = True

            for reference, file_name in self.outputs_map.items():
                file_url = f'{self.job_url}/{reference}'
                file_path = self.work_dir / file_name

                with fetch_session.get(file_url) as file_response:
                    file_response.raise_for_status()

                    self.logger.debug("downloading %s as %s ...", file_url,
                                                                      file_path)

                    with file_path.open(mode='wb') as file_object:
                        for chunk in file_response.iter_content(
                                              chunk_size=self.fetch_chunk_size):
                            file_object.write(chunk)

                self.logger.info("downloaded %s as %s", file_url, file_path)

        self.logger.debug('FETCHED!')
        return self

    def clean(self):
        if not self.job_url:
            raise RuntimeError('no job url!')

        requests.delete(self.job_url)
        self.logger.info("cleaned job %s ...", self.job_url)

        if not self.keep_blobs:
            for blob_service_id in self.blobs_map.values():
                requests.delete(f'{self.url}/blob/{blob_service_id}')

        self.job_url = None
        self.blobs_map = None
        self.logger.debug('CLEANED!')


def make_archive_tar_gz(path, input_names):

    with tarfile.open(path, mode='w:gz', compresslevel=6, dereference=True) \
                                                                    as tar_file:
        logger.debug('new tar archive %s:', path)

        dir_path = path.parent
        for input_name in input_names:

            input_path = dir_path / input_name
            if input_path.exists():
                logger.debug('adding %s as %s ...', input_path, input_name)
                tar_file.add(name=input_path, arcname=input_name)
            else:
                logger.debug('SKIPPING %s!', input_path)

    logger.info('created tar archive %s!', path)

def extract_archive_tar_gz(path):

    with tarfile.open(path) as tar_file:
        logger.debug('extracting tar archive %s ...', path)

        dir_path = path.parent
        # for tar_member in tar_file.getmembers():

            # logger.debug('getting %s, %ib', tar_member.name, tar_member.size)
            # tar_file.extract(tar_member, path=dir_path, set_attrs=False)

        tar_file.extractall(dir_path)

    logger.info('extracted tar archive %s!', path)

def main(*args):
    logger.debug(args)
    url, blob_service, job_service, label, work_dir, submit_args_str, \
                                                        *input_file_names = args

    work_dir_path = Path(work_dir)
    submit_args = dict( entry.split('=')
                                       for entry in submit_args_str.split(',') )

    inputs_tar_name = 'inputs.tar.gz'
    outputs_tar_name = 'outputs.tar.gz'

    inputs_tar_path = work_dir_path / inputs_tar_name
    input_relative_file_names = tuple( Path(name).relative_to(work_dir_path)
                                                  for name in input_file_names )

    make_archive_tar_gz(inputs_tar_path, input_relative_file_names)

    inputs_map = dict(
        inputs = inputs_tar_name,

    )
    outputs_map = dict(
        mirror = 'mirror.out',
        sandbox = 'sandbox.tar.gz',
        outputs = outputs_tar_name,
    )

    sproxy_exec = SProxyExecution(url, blob_service, job_service, label,
          work_dir_path, submit_args, inputs_map, outputs_map, keep_blobs=False)
    try:
        sproxy_exec.prepare().submit().wait().fetch()
    finally:
        sproxy_exec.clean()

    outputs_tar_path = work_dir_path / outputs_tar_name
    extract_archive_tar_gz(outputs_tar_path)

    inputs_tar_path.unlink()
    outputs_tar_path.unlink()

def setup_logging(msg_fmt=logging_fmt, console_lvl=logging_console_lvl,
                                 base_log_path=None, file_lvl=logging_file_lvl):

    base_logger = logging.getLogger()
    base_logger.setLevel(logging.NOTSET)
    logger_formatter = logging.Formatter(fmt=msg_fmt)

    # outputs just launcher log records to console
    console_output = logging.StreamHandler()
    console_output.setLevel(console_lvl)
    console_output.setFormatter(logger_formatter)
    base_logger.addHandler(console_output)

    if base_log_path:
        # logs everything to weekly rotating file
        file_log = logging.handlers.TimedRotatingFileHandler(setup_logging,
                                                      when='W1', backupCount=10)
        file_log.setLevel(file_lvl)
        file_log.setFormatter(logger_formatter)
        base_logger.addHandler(file_log)

import sys

if __name__ == '__main__':
    setup_logging()
    main(*sys.argv[1:])
