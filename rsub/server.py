from flask import Flask, request
from pathlib import Path

from . import conf, utils

all_jobs = conf.JOB_SERVICES
all_blobs = conf.BLOB_SERVICES

def create_app( blob_services=all_blobs.keys(), job_services=all_jobs.keys() ):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__)

    def load_(services, conf_map):
        services_ = utils.string_splitting(services)
        for service in services_:
            services_type, services_args = conf_map[service]
            services_type(service, **services_args).register_in(app)

    load_(blob_services, all_blobs)
    load_(job_services, all_jobs)

    @app.route( '/test', methods=('GET', 'POST') )
    def test():
        response = (
            "args={0.args}\n"
            "base_url={0.base_url}\n"
            "content_encoding={0.content_encoding}\n"
            "content_length={0.content_length}\n"
            "content_md5={0.content_md5}\n"
            "data={0.data}\n"
            "date={0.date}\n"
            "dict_storage_class={0.dict_storage_class}\n"
            "endpoint={0.endpoint}\n"
            "environ={0.environ}\n"
            "files={0.files}\n"
            "form={0.form}\n"
            "full_path={0.full_path}\n"
            "headers={0.headers}\n"
            "host={0.host}\n"
            "host_url={0.host_url}\n"
            "is_secure={0.is_secure}\n"
            "is_xhr={0.is_xhr}\n"
            "json={0.json}\n"
            "method={0.method}\n"
            "path={0.path}\n"
            "query_string={0.query_string}\n"
            "scheme={0.scheme}\n"
            "script_root={0.script_root}\n"
            "url={0.url}\n"
            "url_root={0.url_root}\n"
            "user_agent={0.user_agent}\n"
            "request={0}"
        ).format(request)

        print(response)
        return response

    return app
