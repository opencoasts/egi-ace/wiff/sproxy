from pathlib import Path

from .services.blob import DummyBlobService, LocalBlobService

BLOB_SERVICES = dict(
    # test = (
    #     DummyBlobService, dict()
    # ),
    local = (
        LocalBlobService, dict(
            local_path=Path('/home/centos/'),
        )
    ),
)


from datetime import timedelta

from .services.job import DummyJobService, LocalJobService
from .services.job_sge import SunGridEngineSSHJobService
from .services.job_dirac import DIRACJobService

schism_conf_map = dict(
    bctides = 'bctides.in',
    param = 'param.in',
)

schism_input_map = dict(
    albedo = 'albedo.gr3',
    diffmax = 'diffmax.gr3',
    diffmin = 'diffmin.gr3',
    drag = 'drag.gr3',
    elev = 'elev2D.th',
    hgrid = 'hgrid.gr3',
    hotstart = 'hotstart.in',
    salt_3d = 'SAL_3D.th',
    salt_ic = 'salt.ic',
    schism = 'schism',
    sflux_air = 'sflux/sflux_air_1.001.nc',
    sflux_rad = 'sflux/sflux_rad_1.001.nc',
    temp_3d = 'TEM_3D.th',
    temp_ic = 'temp.ic',
    vgrid = 'vgrid',
    watertype = 'watertype.gr3',
    windrot = 'windrot',

    inputs = 'inputs.tar.gz',
    forcings = 'forcings.tar.gz',
    hotstarts = 'hotstarts.tar.gz',
)

out_ = lambda file: f'outputs/{file}'

schism_output_map = dict(
    mirror = 'mirror.out',
    elev_1 = out_('1_elev.nc'),
    elev_2 = out_('2_elev.nc'),
    hvel_1 = out_('1_hvel.nc'),
    hvel_2 = out_('2_hvel.nc'),
    salt_1 = out_('1_salt.nc'),
    salt_2 = out_('2_salt.nc'),
    temp_1 = out_('1_temp.nc'),
    temp_2 = out_('2_temp.nc'),
    zcor_1 = out_('1_zcor.nc'),
    zcor_2 = out_('2_zcor.nc'),
    hotstart_1800 = out_('1800_hotstart.in'),
    hotstart_3600 = out_('3600_hotstart.in'),

    outputs = 'outputs.tar.gz',
    results = out_('results.tar.gz'),
    hotstarts = out_('hotstarts.tar.gz'),
)

JOB_SERVICES = dict(
    # echo = (
    #     LocalJobService, dict(
    #         debug = True,
    #         exec_args = '/bin/echo {workdir}',

    #         input_map = dict(
    #             readme = 'README.md'
    #         ),

    #         output_map = dict(
    #             readme = 'README.md'
    #         ),

    #         # udocker = 'udocker',
    #         # udocker_args = ('-v "{workdir}":/host', ),
    #         # mpirun = '/usr/lib64/openmpi/bin/mpirun',
    #         # mpirun_args = '-np10',
    #     )
    # ),

    # sleep_600 = (
    #     LocalJobService, dict(
    #         exec_args = '/bin/sleep 600',
    #     )
    # ),

    # sge_ssh = (
    #     SunGridEngineSSHJobService, dict(
    #         ssh_host = 'medusa',

    #         init_script = (
    #             'tar --verbose --extract --file=inputs.tar.gz && '
    #                                                'rm --verbose inputs.tar.gz',
    #             #'ln --verbose --symbolic --no-dereference hgrid.gr3 hgrid.ll',
    #         ),

    #         qsub_args = (
    #             'N oc_{label}',
    #             'A oc_eoschub',
    #             'q fast_medusa',
    #             'l lnecintel=y',
    #             'wd $HOME/{workdir}',
    #             'pe mpi 12',
    #             'R yes',
    #         ),
    #         qsub_script = (
    #             'module load gcc63/openmpi/2.1.0',
    #             'mpirun -np \$NSLOTS $HOME/rsub/bins/pschism_LNEC_WS_GNU_VL',
    #             ''
    #             'tar --verbose --create --gzip --remove-files '
    #                                           '--file=outputs.tar.gz outputs/*',
    #         ),

    #         conf_map = schism_conf_map,
    #         input_map = schism_input_map,
    #         output_map = schism_output_map,
    #     )
    # ),

    egi_dirac = (
        DIRACJobService, dict(
            config = dict(
                rest_url = 'https://dirac5.grid.cyfronet.pl:9178',
                params = dict(
                    grant_type = 'client_credentials',
                    group = 'opencoast_user',
                    setup = 'EGI-Production',
                ),
                capath = '/home/centos/certs/',
                cainfo = '/home/centos/certs/PolishGRIDca.pem',
                sslkey = '/home/centos/certs/.globus/robotuserkey.pem',
                sslcert = '/home/centos/certs/.globus/robotusercert.pem',
            ),
            # token_ttl = timedelta(hours=6),

            jdl = dict(
                Executable = '/bin/bash',
                Arguments = 'schism.sh {remote_path} {job_dir} {exec}',
                StdOut = 'std.out',
                StdError = 'std.err',
                OutputSandbox = {'std.out', 'std.err'},
                JobName = 'oc_{label}',
                Site = {'EGI.INGRID.pt'},
                NumberOfProcessors = 8,
                CPUTime = 600,
            ),
            job_files = {
                'schism.sh': 'scripts/dirac_schism.sh',
            },

            storm_config = dict(
                 rest_url='gftp01.ncg.ingrid.pt:8443',
                 capath='/etc/grid-security/certificates',
            ),

            docker_image = '../data/schism_image.tar.xz',

            input_map = dict(
                inputs = 'inputs.tar.gz',
            ),
            output_map = dict(
                # mirror = 'mirror.out',
                outputs = 'outputs.tar.gz',
            ),
            force_upload_requirements = True,
        )
    ),

)

