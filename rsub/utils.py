import random
from collections.abc import Collection
from functools import wraps
from string import ascii_letters, digits
from time import time

def is_listing(obj):
    return isinstance(obj, Collection) and not isinstance(obj, str)

def tupling(seq_or_obj):
    if seq_or_obj is None:
        return tuple()
    return tuple(seq_or_obj) if is_listing(seq_or_obj) else (seq_or_obj,)

def string_splitting(tuple_or_str, sep=None):
    if tuple_or_str is None:
        return tuple()
    return tuple_or_str.split(sep) if isinstance(tuple_or_str, str) \
                                                               else tuple_or_str

def string_joining(seq_or_str, sep=' '):
    return sep.join(seq_or_str) if is_listing(seq_or_str) else seq_or_str

def string_stripping(seq_or_str, chars=None):
    return tuple( str_.strip(chars) for str_ in tupling(seq_or_str) )

def random_string(length=4, space=ascii_letters+digits):
    return ''.join( random.choice(space) for _ in range(length) )


class Method:

    # use abc instead!
    @staticmethod
    def abstract(method):

        @wraps(method)
        def wrapper(self, *args, **kwargs):
            raise NotImplementedError(
                f"{type(self).__qualname__}.{method.__name__} "
                               "is an abstract method, it must be implemented!")

        return wrapper

    @staticmethod
    def profile(method):

        @wraps(method)
        def wrapper(self, *args, **kwargs):
            marker = random_string()
            name = method.__name__
            self.logger.warning(f" »»» [{marker}] {name} started!")

            begin = time()
            result = method(self, *args, **kwargs)
            duration = time() - begin

            self.logger.warning(
                          f" ««« [{marker}] {name} finished in {duration:.3f}s")
            return result

        return wrapper