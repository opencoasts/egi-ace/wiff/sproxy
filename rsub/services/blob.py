from ..utils import Method
from .core import ServiceBase, ServiceError


class InstanceMappingMixin:

    _instance_map = dict()

    def __init__(self, name, *args, **kwargs):
        super().__init__(name, *args, **kwargs)

        self._instance_map[name] = self

    @classmethod
    def instance(cls, name):
        return cls._instance_map[name]


class BlobService(InstanceMappingMixin, ServiceBase):

    default_prefix = 'blob'

    def register_routes(self):
        blob_url = '/<blob_id>'
        self.add_route(self.add, '', forward='files:blobs', methods='put')
        self.add_route(self.exist, blob_url)
        self.add_route(self.remove, blob_url, methods='delete')

    @Method.abstract
    def add(self, blobs):
        pass

    @Method.abstract
    def exist(self, blob_id):
        pass

    @Method.abstract
    def remove(self, blob_id):
        pass

    @Method.abstract
    def open(self, blob_id):
        pass


from hashlib import sha3_512


class Hashing:

    default_hash_algorithm = sha3_512

    def __init__(self, *args, hash_algorithm=None, **kwargs):
        super().__init__(*args, **kwargs)

        self._hash_algorithm = hash_algorithm or self.default_hash_algorithm

    def _hash(self, file_obj, algorithm=None, whole=True):
        curr_pos = file_obj.tell()

        if whole:
            file_obj.seek(0)

        hash_algorithm = algorithm or self._hash_algorithm
        file_hash = hash_algorithm()

        while True:
            data = file_obj.read(16384)
            if not data:
                break

            file_hash.update(data)

        file_obj.seek(curr_pos)
        return file_hash.hexdigest()


class DummyBlobService(Hashing, BlobService):

    def add(self, blobs):
        return { name: f'{self.name}/{self._hash( file.read() )}'
                                               for name, file in blobs.items() }


import tarfile
from shutil import copyfileobj
from tempfile import SpooledTemporaryFile


class Archiving:

    default_archive_spool_dir = None
    default_archive_spool_size = 10 * 1024 * 1024 # 10MiB

    def __init__(self, *args, archive_spool_size=None,
                                              archive_spool_dir=None, **kwargs):
        super().__init__(*args, **kwargs)

        spool_size_is_defined = archive_spool_size is not None
        self._archive_spool_size = archive_spool_size if spool_size_is_defined \
                                            else self.default_archive_spool_size
        self._archive_spool_dir = archive_spool_dir \
                                               or self.default_archive_spool_dir

    def _archive(self, files):
        tar_spool_file = SpooledTemporaryFile(max_size=self._archive_spool_size,
                                                    dir=self._archive_spool_dir)
        tar_archive = tarfile.open(mode='w', fileobj=tar_spool_file)

        for name, file in files.items():
            tar_info = tar_file.gettarinfo(fileobj=file)
            tar_archive.addfile(tar_info, fileobj=file)

        tar_archive.close()
        tar_spool_file.seek(0)
        return tar_spool_file

    @staticmethod
    def _is_archive_file(name):
        return tar_file.is_tarfile(name)

    @staticmethod
    def _is_archive_obj(obj):
        return isinstance(obj, tarfile.TarFile)


class Compressing:

    default_compression_algorithm = 'xz'

    def __init__(self, *args, compression_algorithm='', **kwargs):
        super().__init__(*args, **kwargs)

        self._compression_algorithm = compression_algorithm or \
                                              self.default_compression_algorithm


from pathlib import Path


class LocalBlobService(Hashing, BlobService):

    default_local_path = lambda name: Path(f'blobs/{name}')

    def __init__(self, *args, local_path:Path=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.local_path = local_path or self.default_local_path(self.name)
        self.local_path.mkdir(parents=True, exist_ok=True)

    def _store_file_blob(self, file):
        blob_hash = self._hash(file)
        blob_path = self.local_path / blob_hash

        file.save( str(blob_path) )
        file.close()

        return blob_hash

    # def _store_archive_blob(self, files):
    #     archive = self._archive(files)
    #     archive_hash = self._hash(archive)
    #     archive_path = self.local_path / archive_hash

    #     with archive, archive_path.open('wb') as archive_file:
    #         copyfileobj(archive, archive_file)

    #     return archive_hash

    def _blob_path(self, blob_id, check=True):
        try:
            int(blob_id, 16)
        except ValueError:
            return None

        blob_path = self.local_path / blob_id
        return blob_path if not check or blob_path.exists() else False

    @Method.profile
    def add(self, blobs):
        return { name: f'{self.name}/{self._store_file_blob(file)}'
                                               for name, file in blobs.items() }

    def exist(self, blob_id):
        if self._blob_path(blob_id):
            return self.empty_response()

    def remove(self, blob_id):
        blob_path = self._blob_path(blob_id)
        if blob_path:
            blob_path.unlink()
            return True

        return blob_path

    def open(self, blob_id):
        blob_path = self._blob_path(blob_id)
        if blob_path:
            return open(blob_path, 'rb')
