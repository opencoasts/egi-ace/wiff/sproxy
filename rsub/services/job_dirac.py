import requests
from collections.abc import Collection
from datetime import datetime, timedelta
from io import BytesIO
from pathlib import Path

from DIRAC.REST_Handler import JobHandler
from Proxy.voms_myproxy import VomsProxy
from StoRM.webdav import WebDAV

from ..utils import random_string
from .blob import BlobService
#from .blob_storm import StoRMBlobService
from .job import JobService, IOBaseMixin, ServiceError


class DIRACMixin:
    pass


# TODO: change prints for logger output
class DIRACJobService(IOBaseMixin, DIRACMixin, JobService):

    def __init__(
        self, *args, config, jdl, job_files, storm_config, token_ttl=None,
        docker_image=None, workdir_path=None, force_upload_requirements=False,
        **kwargs
    ):
        super().__init__(*args, **kwargs)

        self.local_path = workdir_path or self.local_workdir_path()

        self._init_job_handler(config, jdl, job_files, token_ttl)
        self._init_webdav(storm_config)
        self._upload_requirements(docker_image, force_upload_requirements)

        print("%s inited!", self.full_name)

    default_token_ttl = timedelta(hours=6)

    def _init_job_handler(self, config, jdl, job_files, token_ttl):
        self.jdl = jdl
        self._job_handler = JobHandler(**config)
        self._job_handler.files = job_files

        self.local_path.mkdir(parents=True, exist_ok=True)

        self.token_ttl = token_ttl or self.default_token_ttl
        self._renew_token()

    def _renew_token(self):
        self._job_handler.token_init()
        self.token_renew = datetime.now()
        print('requested new token:', self._job_handler.get_token() )

    udocker_url = \
         'https://raw.githubusercontent.com/indigo-dc/udocker/master/udocker.py'

    def _init_webdav(self, config):
        VomsProxy.createVomsProxy('password')
        self.webdav = WebDAV(**config)

        remote_strpath = str( self.remote_workdir_path() )
        if self.webdav.check(remote_strpath):
            print('creating', remote_strpath)
            self.webdav.mkdir(remote_strpath)

    def _upload_requirements(self, docker_image, force):
        if docker_image:
            remote_workdir_path = self.remote_workdir_path()

            udocker_strpath = str(remote_workdir_path/'udocker')
            if force or not self.webdav.check(udocker_strpath):
                print('uploading udocker to', udocker_strpath)
                with BytesIO(requests.get(self.udocker_url).content) as buff:
                    self.webdav.buffer_upload(buff, udocker_strpath)
                print('uploaded udocker to', udocker_strpath)

            docker_image_strpath = str(remote_workdir_path/'docker_image')
            if force or not self.webdav.check(docker_image_strpath):
                print('uploading docker image to', docker_image_strpath)
                self.webdav.upload(docker_image, docker_image_strpath)
                print('uploaded docker image to', docker_image_strpath)

    def _new_job_local_file_path(self):
        new_file_path = lambda: self.local_path / random_string(length=16)
        job_file_path = new_file_path()
        while job_file_path.exists():
            job_file_path = new_file_path()

        return job_file_path

    @property
    def job_handler(self):
        if not self.token_renew or \
                            self.token_renew + self.token_ttl <= datetime.now():
            self._renew_token()

        return self._job_handler

    def submit(self, label, inputs=None, arguments=None):
        job_local_file_path = self._new_job_local_file_path()
        remote_dir_strpath = \
                            str( self.remote_workdir_path(job_local_file_path) )

        try:
            # create work dir
            self.webdav.mkdir(remote_dir_strpath)
            print('creating', remote_dir_strpath)

            # upload inputs into work dir
            inputs_ = inputs or dict()
            for input_ in inputs_.items():

                src_file_obj, dest_file_name = self._handle_input_item(*input_)
                with src_file_obj:

                    input_strpath = f'{remote_dir_strpath}/{dest_file_name}'
                    print('uploading inputs to', input_strpath)
                    self.webdav.buffer_upload(src_file_obj, input_strpath)

            # test_run_strpath = \
            #     str( self.remote_workdir_path(job_local_file_path) /
            #                                                    'inputs.tar.gz' )
            # print('uploading inputs to', test_run_strpath)
            # self.webdav.upload('data/run_test.tar.gz',
            #                                                    test_run_strpath)

            job_dir = job_local_file_path.name
            arguments_ = arguments or dict()


            # format jdl {}
            env_map = dict(
                label = label,
                job_dir = job_dir,
                remote_path = self.remote_workdir_path(),
                **arguments_,
            )
            format_ = lambda v: (
                v.format_map(env_map) if isinstance(v, str) else
                    tuple( map(format_, v) ) if isinstance(v, Collection) else v
            )

            job_jdl = dict( (k, format_(v) ) for k, v in self.jdl.items() )
            print(job_jdl)

            job_id = self.job_handler.submitJob(manifest=job_jdl)
            print('job dir:', job_dir, 'job id:', job_id)
            job_local_file_path.write_text( str(job_id) )

        except:
            self.webdav.delete(remote_dir_strpath)
            raise

        return self.resource_location( str(job_dir) )

    status_state_map = dict(
        Received = JobService.State.waiting,
        Checking = JobService.State.waiting,
        Waiting = JobService.State.waiting,
        Matched = JobService.State.waiting,
        Running = JobService.State.running,
        Completed = JobService.State.running,
        Failed = JobService.State.exited,
        Done = JobService.State.exited,
        Killed = JobService.State.exited,
        Stalled = JobService.State.exited,
    )

    def _remote_job_id(self, job_id):
        job_local_file_path = self.local_path / job_id
        return int( job_local_file_path.read_text() )

    def status(self, job_id):
        remote_job_id = self._remote_job_id(job_id)
        job_status = self.job_handler.getJobStatus(remote_job_id)
        print('job id:', remote_job_id, 'job status:', job_status)
        return self.status_state_map[job_status]

    def fetch(self, job_id, reference):

        if reference == 'sandbox':
            remote_job_id = self._remote_job_id(job_id)
            sandbox_buff = \
                   BytesIO(self.job_handler.getJobOutput_buffer(remote_job_id) )
            return self.stream_file_obj(sandbox_buff)

        output_file_name = self.output_map.get(reference)
        if not output_file_name:
            raise ServiceError(f"{output_key}: invalid output name!")

        output_strpath = \
                   str( self.remote_workdir_path() / job_id / output_file_name )

        output_buffer = BytesIO()
        self.webdav.buffer_download(output_strpath, output_buffer)
        output_buffer.seek(0)

        return self.stream_file_obj(output_buffer)

    def close(self, job_id):
        job_local_file_path = self.local_workdir_path() / job_id
        remote_job_id = self._remote_job_id(job_id)
        job_remote_strpath = \
                            str( self.remote_workdir_path(job_local_file_path) )

        job_local_file_path.unlink()
        self.job_handler.deleteJob( int(remote_job_id) )
        self.webdav.delete(job_remote_strpath)

        return self.State.closed
