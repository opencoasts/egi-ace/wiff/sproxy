import fabric
from datetime import datetime, timedelta
from pathlib import Path

from ..utils import Method, tupling, string_joining, random_string
from .job import JobService, ExecBaseMixin, IOBaseMixin


class SSHMixin(ExecBaseMixin, IOBaseMixin):

    # _connections_cache = dict()

    def __init__(self, *args, ssh_host, **kwargs):
        super().__init__(*args, **kwargs)

        self._ssh_host = ssh_host
        self._ssh = fabric.Connection(ssh_host)
        self._sftp = None

        # _connections_cache[ssh_host] = (self.ssh, self.sftp)

    def _check_ssh(self):

        def open_ssh():
            self._ssh.close()
            self.logger.info('new %r ssh connection', self._ssh_host)
            self._ssh.open()

        if not self._ssh.is_connected:
            return open_ssh()

        try:
            self._ssh.run('pwd', hide=True)
        except:
            self.logger.exception('%r ssh connection is broken', self._ssh_host)
            open_ssh()

    def _check_sftp(self):
        if self._sftp:
            try:
                self._sftp.getcwd()
            except OSError:
                self.logger.exception('%r sftp connection is broken',
                                                                 self._ssh_host)
                self._sftp.close() # just in case
                self._ssh._sftp = None # clears wrapped ssh's internal sftp obj
            else:
                return

        self.logger.info('new %r sftp connection', self._ssh_host)
        self._sftp = self._ssh.sftp()

    @classmethod
    def format_script(cls, *lines, as_cmd=False, wd=None, fmt_map=None):
        if fmt_map:
            lines = cls.format_fields(*lines, **fmt_map)

        if as_cmd:
            cmd_str = string_joining(lines, '; ')
            script = f' {cmd_str}; ' if wd else f' {cmd_str} '
        else:
            script_sep = '\n  ' if wd else '\n'
            script_str = script_sep.join(lines)
            script = f'\n{script_str}\n' if wd else script_str

        return f'cd "{wd}" && {{{script}}}' if wd else script

    _default_ssh_run_timeout = timedelta(seconds=10)

    def ssh_run(self, command, check=True, timeout=None):
        self._check_ssh()
        run = self._ssh.run(
            command,
            echo=True,
            hide = True,
            warn = not check,
            timeout = self._default_ssh_run_timeout.total_seconds(),
        )

        self.logger.debug("%s > %s", self._ssh_host, command)
        return run

    _ssh_create_dir_cmd = 'mkdir --parents --verbose {path}'

    def ssh_create_dir(self, path):
        mkdir_cmd = self._ssh_create_dir_cmd.format(path=path)
        mkdir_run = self.ssh_run(mkdir_cmd)

        self.logger.info("[ssh] created dir %s", path)
        return mkdir_run

    _ssh_move_cmd = 'mv --verbose {src_path} {dest_path}'

    def ssh_move(self, src_path, dest_path):
        mv_cmd = self._ssh_move_cmd.format(src_path=src_path,
                                                            dest_path=dest_path)
        mv_run = self.ssh_run(mv_cmd)

        self.logger.info("[ssh] moved %s to %s", src_path, dest_path)
        return mv_run

    _ssh_remove_cmd = 'rm --recursive --force --verbose {path}'

    def ssh_remove(self, path):
        path_ = Path(path)
        if path_ in ( Path(), Path.home() ):
            raise ValueError(f'cannot remove {path_}!')

        rm_cmd = self._ssh_remove_cmd.format(path=path_)
        rm_run = self.ssh_run(rm_cmd)

        self.logger.info("[ssh] removed %s", path_)
        return rm_run

    def ssh_create_link(self, path, target):
        ln_cmd = self._ssh_create_link_cmd.format(path=path, target=target)
        ln_run = self.ssh_run(ln_cmd)

        self.logger.info("[ssh] linked %s to %s", path, target)
        return ln_run

    _ssh_read_link_cmd = 'readlink --no-newline --silent {path}'

    def ssh_read_link(self, path):
        readlink_cmd = self._ssh_read_link_cmd.format(path=path)
        readlink_run = self.ssh_run(readlink_cmd, check=False)

        self.logger.info("[ssh] read link %s", path)
        if readlink_run:
            readlink_stdout = readlink_run.tail('stdout', count=1).strip()
            return Path(readlink_stdout)

    _ssh_create_text_file_cmd = 'echo -e "{text}" {mode} {path}'

    def ssh_create_text_file(self, text, path, truncate=True):
        mode = '>' if truncate else '>>'
        echo_cmd = self._ssh_create_text_file_cmd.format(text=text, mode=mode,
                                                                      path=path)
        echo_run = self.ssh_run(echo_cmd)
        mode_str = 'trancated' if truncate else 'appended'

        self.logger.info("[ssh] created text file %s: %ib (%s)", path,
                                                            len(text), mode_str)
        return echo_run

    _ssh_create_link_cmd = 'ln --symbolic --no-dereference --force --verbose ' \
                                                               '{target} {path}'

    def sftp_create_dir(self, path):
        paths_not_found = list()
        paths_to_check = (path, *path.parents)[:-1]
        for path_ in paths_to_check:
            try:
                self._sftp.stat(str(path_) )
            except FileNotFoundError:
                paths_not_found.append(path_)
            else:
                break

    def sftp_create_text_file(self, text, path, truncate=True):
        raise NotImplementedError

    def sftp_send(self, src_file_obj, dest_file_path):
        self._check_sftp()
        with src_file_obj:
            self._sftp.putfo(src_file_obj, str(dest_file_path) )

    def sftp_send_input(self, input_key, input_reference, dest_dir_path):
        src_file_obj, dest_file_name = self._handle_input_item(input_key,
                                                                input_reference)
        dest_file_path = dest_dir_path / dest_file_name
        self.sftp_send(src_file_obj, dest_file_path)

    def sftp_open(self, src_file_path):
        self._check_sftp()
        return self._sftp.open(str(src_file_path) )

    def sftp_open_output(self, output_key, src_dir_path):
        output_file_name = self.output_map.get(output_key)
        if not output_file_name:
            raise ServiceError(f"{output_key}: invalid output name!")

        src_file_path = src_dir_path / output_file_name
        return self.sftp_open(src_file_path)


class SunGridEngineSSHJobService(SSHMixin, JobService):

    def __init__(self, *args, qsub_script, qsub_args=None, workdir_path=None,
                                                    init_script=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.qsub_script = tupling(qsub_script)
        self.qsub_args = qsub_args
        self.workdir_path = workdir_path or self.remote_workdir_path()
        self.init_script = tupling(init_script)

    _base_qsub_args = (
        'terse',
    )

    def _format_qsub_script(self, fmt_map):
        qsub_args = self.qsub_args + self._base_qsub_args
        qsub_script_args = ( f'#$ -{arg}' for arg in qsub_args )

        return '\n'.join(
            self.format_fields(
                '#!/usr/bin/env bash',
                '',
                *qsub_script_args,
                '',
                *self.qsub_script,

                **fmt_map,
            )
        )

    _submit_qsub_cmd = 'qsub {sub_file}'

    @Method.profile
    def submit(self, label, *, inputs=None, confs=None):
        timestamp_utc = datetime.utcnow().strftime('%Y-%m-%dT%H-%M-%S.%f')
        unique_dirname = f'{timestamp_utc}_{random_string()}'
        submit_dirpath = self.workdir_path / label / unique_dirname
        self.ssh_create_dir(submit_dirpath)

        script_fmt_map = dict(
            label = label,
            workdir = submit_dirpath,
        )

        try:
            # handle confs
            confs_ = confs or dict()
            for conf_key, conf in confs_.items():
                conf_file_name = self.conf_map.get(conf_key)
                if not conf_file_name:
                    raise ServiceError(f"{conf_key}: invalid conf name!")

                conf_text = string_joining(conf, '\n')
                conf_path = submit_dirpath / conf_file_name
                self.ssh_create_text_file(conf_text, conf_path)

            # handle input files
            inputs_ = inputs or dict()
            for in_key, in_ref in inputs_.items():
                self.sftp_send_input(in_key, in_ref, submit_dirpath)

            if self.init_script:
                init_script = self.format_script(*self.init_script, as_cmd=True,
                                      wd=submit_dirpath, fmt_map=script_fmt_map)
                init_run = self.ssh_run(init_script)

            qsub_script = self._format_qsub_script(script_fmt_map)
            qsub_script_path = submit_dirpath / 'submission.sh'
            self.ssh_create_text_file(qsub_script, qsub_script_path)

        except:
            self.ssh_remove(submit_dirpath)
            raise

        qsub_cmd = self._submit_qsub_cmd.format(sub_file=qsub_script_path)
        qsub_run = self.ssh_run(qsub_cmd)

        job_id = int( qsub_run.tail('stdout', count=1).strip() )
        job_id_link_path = self.workdir_path / str(job_id)
        job_id_link_target_path = submit_dirpath.relative_to(self.workdir_path)
        self.ssh_create_link(job_id_link_path, job_id_link_target_path)
        self.logger.warning("submitted %r job #%i @ %s", label, job_id,
                                                               qsub_script_path)

        return self.resource_location(str(job_id),
            dict(
                id = str(job_id),
                label = label,
                path = str(job_id_link_path),
            )
        )

    def _check_job_id(self, job_id):
        int(job_id)

    def _job_link(self, job_id):
        self._check_job_id(job_id)
        return self.workdir_path / job_id

    def _job_dir(self, job_id):
        return self.ssh_read_link( self._job_link(job_id) )

    def _job_exists(self, job_id):
        return bool(self._job_dir(job_id) )

    def _job_path(self, job_id):
        job_dir = self._job_dir(job_id)
        if job_dir:
            return self.workdir_path / job_dir

    _job_state_qstat_cmd = 'qstat -j {job_id}'

    def _job_state(self, job_id):
        if self._job_exists(job_id):
            qstat_cmd = self._job_state_qstat_cmd.format(job_id=job_id)
            if self.ssh_run(qstat_cmd, check=False):
                return self.State.running

            return self.State.exited

    def status(self, job_id):
        return self._job_state(job_id)

    def fetch(self, job_id, reference):
        if self._job_state(job_id) == self.State.exited:
            file_obj = self.sftp_open_output(reference, self._job_link(job_id) )
            self.logger.warning("requested %r from job #%s", reference, job_id)
            return self.stream_file_obj(file_obj)

    _close_qdel_cmd = 'qdel {job_id}'

    def close(self, job_id, keep_workdir=False):
        job_path = self._job_path(job_id)
        if not job_path:
            return

        qdel_cmd = self._close_qdel_cmd.format(job_id=job_id)
        qdel = self.ssh_run(qdel_cmd, check=False)

        if not keep_workdir:
            self.ssh_remove(job_path)

        self.ssh_remove(self._job_link(job_id) )

        workdir_action = 'kept' if keep_workdir else 'deleted'
        self.logger.warning("closed job #%s (%s %s)", job_id, job_path,
                                                                 workdir_action)
        return self.State.closed
