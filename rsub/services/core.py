from collections import ChainMap
from flask import Blueprint, request, abort, jsonify, Response
from io import IOBase
from itertools import starmap
from functools import wraps
from logging import getLogger

from ..utils import Method

base_logger = getLogger(__name__)


class ServiceBase:

    logger = base_logger.getChild(__qualname__)

    default_prefix = None
    default_blueprint_kwargs = dict()

    def __init__(self, name, *args, import_name=None, prefix=None,
                                         bp_kwargs=None, debug=False, **kwargs):
        self.debug = debug

        prefix_ = prefix or self.default_prefix
        full_name = f'{prefix_}.{name}' if prefix_ else name
        full_url_prefix = f'/{prefix_}/{name.lower()}' if prefix_ \
                                                         else f'/{name.lower()}'

        service_kwargs = dict(
            name = full_name,
            import_name = import_name or self.__module__,
            url_prefix = full_url_prefix,
        )

        bp_kwargs_ = ChainMap(service_kwargs, bp_kwargs or dict(),
                                                  self.default_blueprint_kwargs)
        self.blueprint = Blueprint(**bp_kwargs_)
        self.register_routes()

        self.name = name
        self.full_name = f'{type(self).__qualname__}.{name}'
        self.logger = getLogger(self.full_name)

        self.logger.warning("%s inited!", self.full_name)

        super().__init__(*args, **kwargs)

    def register_in(self, app):
        app.register_blueprint(self.blueprint)
        app.logger.getChild(self.full_name)

    def add_route(self, func, *rules, forward='', methods='', **route_kwargs):
        '''similar to Flask.Blueprint.add_url_rule signature'''

        def dict_from_str(items_str):
            partitioned_items = ( item_str.partition(':')
                                             for item_str in items_str.split() )
            dict_item = lambda key, _, value: (key, value) if value \
                                                                 else (key, key)

            return dict( starmap(dict_item, partitioned_items) )

        forward_dict = dict_from_str(forward)

        @wraps(func)
        def wrapper(*args, **kwargs):
            extra_kwargs = self._func_signature_handler(forward_dict)

            try:
                func_ret = func(*args, **kwargs, **extra_kwargs)
            except ServiceError as error:
                return str(error)

            return self._func_return_handler(func_ret)

        methods_ = methods.split() or None
        for rule in rules:
            self.blueprint.add_url_rule(rule, methods=methods_,
                                              view_func=wrapper, **route_kwargs)

    def _func_signature_handler(self, forward_dict):
        forward_kwargs = { arg_name: getattr(request, req_member)
                              for req_member, arg_name in forward_dict.items() }
        args_kwargs = request.args.to_dict()
        json_kwargs = request.json or dict()

        return ChainMap(forward_kwargs, args_kwargs, json_kwargs)

    def _func_return_handler(self, func_ret):
        if func_ret is None:
            abort(404)

        func_ret_is_ = lambda kind: isinstance(func_ret, kind)

        # if func_ret_is_(Response):
        #     return func_ret

        if func_ret_is_(IOBase):
            return self.stream_file(func_ret)

        return func_ret

    default_stream_file_obj_buffer_size = 16 * 1024 # 16KiB

    def stream_file_obj(self, file_obj, whole=True, buffer_size=None):
        if whole:
            file_obj.seek(0)

        buffer_size_ = buffer_size or self.default_stream_file_obj_buffer_size
        def file_generator():
            with file_obj as file:
                while True:
                    data = file.read(buffer_size_)
                    if not data:
                        break
                    yield data

        return self.stream_generator(file_generator)

    @staticmethod
    def stream_generator(gen_obj, mimetype='application/octet-stream'):
        return Response(gen_obj(), mimetype=mimetype)

    @staticmethod
    def resource_location(location, content=None):
        return content or '', 201, dict(location=location) # http code 201: Created

    @staticmethod
    def empty_response(headers=None):
        return '', 204, headers # No Content

    @Method.abstract
    def register_routes(self):
        pass

class ServiceError(Exception):
    pass
