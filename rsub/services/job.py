from ..utils import Method
from .core import ServiceBase, ServiceError


class JobService(ServiceBase):

    from ..common import JobState as State

    default_prefix = 'job'

    def register_routes(self):
        job_url  = '/<job_id>'
        self.add_route(self.submit, '/<label>', methods='post')
        self.add_route(self.status, job_url)
        self.add_route(self.fetch, f'{job_url}/<reference>')
        self.add_route(self.close, job_url, methods='delete')

    local_workdir_path = lambda self: Path(f'jobs/{self.name}')
    remote_workdir_path = lambda self, workdir=None: \
                             'sproxy' / ( workdir or self.local_workdir_path() )

    @Method.abstract
    def submit(self, label, inputs=None, arguments=None):
        pass

    @Method.abstract
    def status(self, job_id):
        pass

    @Method.abstract
    def fetch(self, job_id, reference):
        pass

    @Method.abstract
    def close(self, job_id):
        pass


class DummyJobService(JobService):

    def submit(self, label, inputs=None, arguments=None):
        self.logger.info("job %s submitted!", label)
        return dict(
            label = label,
            job_id = 'job_id',
            inputs = inputs,
            arguments = arguments,
        )

    def status(self, job_id):
        self.logger.info("job #%i status!", job_id)
        return dict(
            job_id = job_id,
            status = 'status',
        )

    def fetch(self, job_id, reference):
        self.logger.info("fetch %s from job #%i!", reference, job_id)
        return dict(
            job_id = job_id,
            reference = reference
        )

    def close(self, job_id):
        self.logger.info("job #%i closed!", job_id)
        return dict(
            job_id = job_id,
        )


from ..utils import string_splitting


class ExecBaseMixin:

    def __init__(self, *args, middleware=(), **kwargs):
        super().__init__(*args, **kwargs)

        self._middleware = middleware

    def middleware(self):
        return ()

    @staticmethod
    def format_fields(*parts, **fmt_map):
        return tuple( part.format(**fmt_map) for part in parts )

    # @Method.abstract
    # def run(self):
    #     pass


import subprocess
from pathlib import Path


class LocalExecMixin(ExecBaseMixin):

    def run(self, workdir, *args, use_udocker=False, use_mpirun=False):
        proc_args = format_fields_fields(*self.middleware(), *args, workdir=workdir)
        proc = subprocess.Popen(proc_args, cwd=workdir,
                             stdin=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        self.logger.warning("running [%i] %s @ %s", proc.pid,
                                                   ' '.join(proc_args), workdir)
        return proc


from .blob import BlobService

class IOBaseMixin:

    def __init__(self, *args, conf_map=None, input_map=None, output_map=None,
                                                                      **kwargs):
        super().__init__(*args, **kwargs)

        self.conf_map = conf_map or dict()
        self.input_map = input_map or dict()
        self.output_map = output_map or dict()

    io_input_services = BlobService
    io_reference_separator = '/'

    def _handle_input_item(self, key, reference):
        input_file_name = self.input_map.get(key)
        if not input_file_name:
            raise ServiceError(f"{key}: invalid input key!")

        service_name, _, input_id = reference.rpartition(
                                                    self.io_reference_separator)

        try:
            service = self.io_input_services.instance(service_name)
        except KeyError:
            raise ServiceError(f"{service_name}: invalid service name!")

        if not service.exist(input_id):
            raise ServiceError(f"{input_id}: invalid input id!")

        input_file_handle = service.open(input_id)
        return input_file_handle, input_file_name

    # @Method.abstract
    # def send(self, input_key, input_reference, dest_path):
    #     pass

    # @Method.abstract
    # def open(self, output_key, src_path):
    #     pass


from io import FileIO
from shutil import copyfileobj


class LocalIOMixin(IOBaseMixin):

    def send(self, input_key, input_reference, dest_path):
        src_handle, dest_file = self._handle_input_item(input_key,
                                                                input_reference)
        dest_file_path = dest_path / dest_file

        # if isinstance(src_handle, FileIO):
        #     dest_file_path.symlink_to(src_handle.name)

        #else:
        with src_handle, dest_file_path.open('wb') as dest_handle:
            copyfileobj(src_handle, dest_handle)

    def open(self, output_key, src_path):
        try:
            output_file = self.output_map[output_key]
        except KeyError:
            raise ServiceError(f"{output_key}: invalid output name!")

        src_file_path = src_path / output_file
        return src_file_path.open('rb')


import os
import signal
import shutil
from datetime import datetime


class LocalJobService(LocalExecMixin, LocalIOMixin, JobService):

    default_workdir_path = Path('jobs')

    def __init__(self, *args, exec_args=(), workdir_path=None, **kwargs):
        super().__init__(*args, **kwargs)

        self.exec_args = string_splitting(exec_args)

        self.workdir_path = workdir_path or self.default_workdir_path
        self.proc_path = self.workdir_path / '__proc__'
        self.proc_path.mkdir(parents=True, exist_ok=True)

    @Method.profile
    def submit(self, label, *, inputs=dict(), arguments=() ):
        submit_dirname = datetime.now().isoformat(timespec='microseconds')
        submit_dirpath = self.workdir_path / label / submit_dirname
        submit_dirpath.mkdir(parents=True)

        # handle input files
        for in_key, in_ref in inputs.items():
            self.send(in_key, in_ref, submit_dirpath)

        # run executable
        exec_proc = self.run(submit_dirpath, *self.exec_args, *arguments)
        exec_proc_path = self.proc_path / str(exec_proc.pid)
        exec_proc_path.symlink_to(submit_dirpath)

        return str(exec_proc.pid)

    def _job_path(self, job_id):
        return self.proc_path / str(job_id)

    def _job_state(self, job_id):
        if self._job_path(job_id).is_symlink():
            try:
                os.kill(job_id, 0)
            except OSError:
                return self.State.exited

            return self.State.running

    def status(self, job_id):
        return self._job_state(job_id)

    @Method.profile
    def fetch(self, job_id, reference):
        if self._job_state(job_id) == self.State.exited:
            return self.open( reference, self._job_path(job_id) )

    @Method.profile
    def close(self, job_id, delete=True):
        job_state = self._job_state(job_id)

        if job_state is None:
            return

        if job_state == self.State.running:
            try:
                os.kill(job_id, signal.SIG_DFL)
            except OSError:
                pass

        job_path = self._job_path(job_id)

        if delete:
            job_path_resolved = job_path.resolve()
            shutil.rmtree(job_path_resolved, ignore_errors=True)

        job_path.unlink()

        return self.State.closed
