#!/usr/bin/env bash

set -ex

INPUT_PATH=${1:?"input path must be provided"}
JOB_DIR=${2:?"job dir must be provided"}
BIN_NAME=${3:?"binary name must be provided"}
PROC_COUNT=${4:-8}

UDOCKER=udocker
DOCKER_IMAGE=docker_image
DOCKER_IMAGE_NAME=opencoasts/schism:centos7-1

INPUT_FILE=inputs.tar.gz
OUTPUT_FILE=outputs.tar.gz

#SRM_PATH='srm://srm01.ncg.ingrid.pt:8444/srm/managerv2?SFN=/opencoast.eosc-hub.eu/opencoastdisk/$INPUT_PATH'
WEBDAV_PATH=https://gftp01.ncg.ingrid.pt:8443/opencoastdisk/$INPUT_PATH
#DFS_PATH=/gstore/t2others/opencoast.eosc-hub.eu/$INPUT_PATH
COPY_TOOL=gfal-copy

export X509_CERT_DIR=/etc/grid-security/certificates

# test current environment to get files in output sandbox
voms-proxy-info --all
#env

mkdir -vp $JOB_DIR

function fetch {
    local output=${2:-$(basename $1)}
    $COPY_TOOL $WEBDAV_PATH/$1 $output # ||
#         $COPY_TOOL $SRM_PATH/$1 $output ||
#         cp $DFS_PATH/$1 $output
}

fetch $UDOCKER
fetch $DOCKER_IMAGE
fetch $JOB_DIR/$INPUT_FILE

#[ -f $DOCKER_IMAGE.xz ] && \
#                xz --decompress --verbose --threads=$PROC_COUNT $DOCKER_IMAGE.xz

tar --extract --verbose --file=$INPUT_FILE --directory=$JOB_DIR

python $UDOCKER install

# import image
python $UDOCKER images | grep -q $DOCKER_IMAGE_NAME ||
    python $UDOCKER load -i $DOCKER_IMAGE
python $UDOCKER ps | grep -q schism ||
    python $UDOCKER create --name=schism $DOCKER_IMAGE_NAME

#python $UDOCKER setup --execmode=P1 schism
python $UDOCKER run -v "$PWD/$JOB_DIR:/_host" -u schism schism \
                                                mpirun -np $PROC_COUNT $BIN_NAME

# copy output files
tar --create --verbose --gzip --directory=$JOB_DIR --file=$OUTPUT_FILE \
                                                             mirror.out outputs/

function send {
    local output=${2:-$(basename $1)}
    $COPY_TOOL $1 $WEBDAV_PATH/$JOB_DIR/$output # ||
#         $COPY_TOOL $1 $SRM_PATH/$output
}

send $OUTPUT_FILE
