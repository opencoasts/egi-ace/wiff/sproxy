# remote submission (rsub)
A remote submission service, built as a [flask][flask home] app.

## Server setup and execution

### Configuration (`rsub/conf.py`)
Each `*_SERVICES`' entry results in a service instance, defined by a **name**
(_string_), its **type** (_service class_) and **`__init__()` arguments**
(_mapping object_).
Each instance binds to its own URL prefix, by default, `/job/<name>`.

    name = (
        type, dict(
            init_argA = valueA
            init_argB = valueB
            ...
            init_argN = valueN
        )
    ),

#### Blob services definition
The blob services definition is made in `conf.py`'s `BLOB_SERVICES` (*mapping
object*).

    BLOB_SERVICES = dict(
        local = (
            LocalBlobService, dict()
        ),
    )

The above definition will provide one blob services, `local` available at the
URL prefix `/blob/local`.

#### Job services definition
The job services definition is made in `conf.py`'s `JOB_SERVICES` (_mapping
object_).
The `input_map` and `output_map` arguments are common among different job
service types, and allow to specify which and where files are used in a job.
This predefined mapping of files (_reference_: _path_) makes the usage simpler
and safer, by not exposing fine details to users.

> The are some tags which may be used in arguments' value to provide some degree
of flexibility, being then converted at the appropriate time.
> The available tags are:
> - {workdir}: working directory path
> - {label}: request label

    JOB_SERVICES = dict(
        echo = (
            LocalJobService, dict(
                exec_path = '/bin/echo',
                exec_args = '{label} @ {workdir}',

                input_map = dict(
                    readme = 'README.md',
                ),

                output_map = dict(
                    readme = 'README.md',
                ),
            )
        ),

        sleep_600 = (
            LocalJobService, dict(
                exec_path = '/bin/sleep',
                exec_args = '600',
            )
        ),
    )

The above definition will provide two job services, `echo` available at the URL
prefix `/job/echo` and `sleep_600` at `/job/sleep_600`.

> It's possible to list all the available URLs using the flask `routes` command.

### Run server
> The built-in server is **NOT** meant for production, **ONLY** for testing!
> The flask documentation covers [this topic][flask deployment].

The `server.py` module defines the `create_app(blob_services=all_blobs,
job_services=all_jobs)` function, an app factory which, by default, loads
all services defined in `conf.py` or, if `*_services` arguments are provided
(a single string with services separated by spaces), allows to specify the
desired ones.

    $ FLASK_APP="rsub.server" python -m flask run

> To load only a subset of the services defined:
> `FLASK_APP="rsub.server:create_app('blob_service1, ..., blob_serviceN',
'job_service1, ..., job_serviceN')`".

## Client usage
> The next examples make use of [HTTPie][httpie home].

The content of a request **MUST** be valid JSON!
HTTPie [makes it easy][httpie json].

### Blob service methods

#### Add | _POST_
    $ http --form POST host[:port]/blob/<service> blob_name@/path/to/file

> `<service>` must be the name defined in the configuration

    HTTP/1.0 200 OK
    Content-Length: 148
    Content-Type: application/json
    Date: Tue, 06 Aug 2019 17:35:39 GMT
    Server: Werkzeug/0.15.4 Python/3.7.3

    {
        "blob_name": "6eebb4995e8c0bfe1e5ef244e347a4453b9d0e9bb9f9040d6b543a9db4c098d0998d3b81ad2d8a37c0329716465c0b343eac48b520b3ca01408cf63ee7741c6e"
    }

#### Exist | _GET_
    $ http host[:port]/blob/<service>/<blob_id>

#### Remove | _DELETE_
    $ http DELETE host[:port]/blob/<service>/<blob_id>

### Job service methods

#### Submit | _POST_
    $ http POST host[:port]/job/<service>/<label> inputs:='[]'

> The `inputs` must list file blob hashes (sha3_512) previously uploaded.

#### Status | _GET_
    $ http host[:port]/job/<service>/<job_id>

#### Fetch | _GET_
    $ http host[:port]/job/<service>/<job_id>/<reference>

#### Close | _DELETE_
    $ http DELETE host[:port]/job/<service>/<job_id>

### Client utility (`client.py`)
This utility streamlines and eases the usage of the service. It defines a class
`SProxyExecution` which provides the methods `prepare`, `submit`, `wait`,
`fetch` and `clean`, combining blob and job services in a clean interface.
It also works as a CLI tester:

    $ python -m client <services url> <blob service> <job service> <job label> <work dir> <work dir files>

[flask home]: https://palletsprojects.com/p/flask "Flask Homepage"
[flask deployment]: https://flask.palletsprojects.com/en/1.1.x/deploying "Flask
Deployment Options"
[httpie home]: https://httpie.org "HTTPie Homepage"
[httpie json]: https://httpie.org/doc#non-string-json-fields "HTTPie Non-string
JSON fields"
